//
//  ServiceExecuterProtocol.swift
//  ImageGallery
//
//  Created by Murtuza Saify on 05/11/17.
//  Copyright © 2017 Saify. All rights reserved.
//
struct ServiceConstants {
    static var baseURLPath = "https://api.flickr.com/services"
}

enum ServiceEndpoints: String {
    case publicFeeds = "/feeds/photos_public.gne"
    
    var absolutePath: String {
        return ServiceConstants.baseURLPath.appending(self.rawValue)
    }
}

// created a protocol for service executer so as to facilitate mocking of api responses
protocol ServiceExecuterProtocol {
    func fetchFeedsWithParams(_ params: [String: Any], completion: @escaping([Feed]?, Error?) -> Void)
}
