//
//  ServiceManager.swift
//  ImageGallery
//
//  Created by Murtuza Saify on 04/11/17.
//  Copyright © 2017 Saify. All rights reserved.
//

import UIKit
import Alamofire

// DefaultServiceExecuter confirms to ServiceExecuterProtocol and does the task of executing services as declared in ServiceExecuterProtocol protocol
class DefaultServiceExecuter: ServiceExecuterProtocol {

    func fetchFeedsWithParams(_ params: [String: Any], completion: @escaping([Feed]?, Error?) -> Void) {

        if let url = URL(string: ServiceEndpoints.publicFeeds.absolutePath) {
            var formattedParams = params
            formattedParams["format"] = "json"
            formattedParams["nojsoncallback"] = 1
            Alamofire.request(url, method: .get, parameters: formattedParams, encoding: URLEncoding.default, headers: nil).validate().responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching feeds: \(String(describing: response.result.error))")
                    completion(nil, response.result.error)
                    return
                }
                guard let value = response.result.value as? [String: Any] else {
                        print("Malformed data received from fetchFeeds")
                        completion(nil, nil)
                        return
                }
                if let items = value["items"] as? [[String: AnyObject]] {
                    var feeds = [Feed]()
                    for item in items {
                        let feed = Feed(item)
                        feeds.append(feed)
                    }
                    completion(feeds, nil)
                }
            }
        }
    }
}
