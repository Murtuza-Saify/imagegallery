//
//  Feed.swift
//  ImageGallery
//
//  Created by Murtuza Saify on 04/11/17.
//  Copyright © 2017 Saify. All rights reserved.
//

import UIKit

class Feed: NSObject {
    var title: String?
    var link: URL?
    var mediaURL: URL?
    var date: Date?
    var feedDescription: String?
    var published: Date?
    var author: String?

    init(_ dictionary: [String: AnyObject]) {
        super.init()

        self.title = dictionary["title"] as? String
        self.feedDescription = dictionary["description"] as? String
        self.author = dictionary["author"] as? String
        self.link = URL(string: dictionary["link"] as? String ?? "")
        if let mediaInfo = dictionary["media"] as? [String: AnyObject] {
            self.mediaURL = URL(string: mediaInfo["m"] as? String ?? "")
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        self.date = dateFormatter.date(from: dictionary["date_taken"] as? String ?? "")
        self.published = dateFormatter.date(from: dictionary["published"] as? String ?? "")
    }
}
