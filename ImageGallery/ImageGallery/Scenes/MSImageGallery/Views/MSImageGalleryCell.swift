//
//  MSImageGalleryCell.swift
//  ImageGallery
//
//  Created by Murtuza Saify on 05/11/17.
//  Copyright © 2017 Saify. All rights reserved.
//

import UIKit
import SDWebImage

class MSImageGalleryCell: UICollectionViewCell {
    
    @IBOutlet fileprivate weak var imageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.layer.cornerRadius = 5
    }

    func configureWithTitle(_ title: String, imageURL: URL, onImageLoad: @escaping () -> Void) {
        self.titleLabel.text = title
        self.imageView.sd_setImage(with: imageURL) { (image, error, type, url) in
            if type == .none {
                onImageLoad()
            }
        }
    }

    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        relayoutAsPerContent()
        setNeedsLayout()
        layoutIfNeeded()
        var newFrame = layoutAttributes.frame
        var fittingSize = UILayoutFittingCompressedSize
        fittingSize.width = layoutAttributes.size.width
        
        let contentView: UIView = self.contentView
        newFrame.size.height = contentView.systemLayoutSizeFitting(fittingSize, withHorizontalFittingPriority: UILayoutPriority(rawValue: 1000), verticalFittingPriority: UILayoutPriority(rawValue: 250)).height
        newFrame.size.width = fittingSize.width
        layoutAttributes.frame = newFrame
        return layoutAttributes
    }

    func relayoutAsPerContent() {

        if let image = SDImageCache.shared().imageFromCache(forKey: self.imageView.sd_imageURL()?.absoluteString) {
            let imageSize = image.size
            let aspectRatio = imageSize.height/imageSize.width
            let consideredWidth = imageSize.width < imageView.bounds.width ? imageSize.width : imageView.bounds.width
            self.imageViewHeightConstraint.constant = consideredWidth * aspectRatio
        }
        self.updateConstraintsIfNeeded()
    }
}
