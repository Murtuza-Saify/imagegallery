//
//  MockServiceExecuter.swift
//  ImageGalleryTests
//
//  Created by Murtuza Saify on 05/11/17.
//  Copyright © 2017 Saify. All rights reserved.
//
import Foundation

enum MockState {
    case validData
    case error
}

class MockServiceExecuter {
    
    var mockState: MockState = .validData
}

// MockServiceExecuter to mock reponses for ServiceExecuter
extension MockServiceExecuter: ServiceExecuterProtocol {
    func fetchFeedsWithParams(_ params: [String : Any], completion: @escaping ([Feed]?, Error?) -> Void) {
        switch self.mockState {
        case .validData:
            if let sampleFeedPath = Bundle(for: ImageGalleryTests.classForCoder()).path(forResource: "MockFeed", ofType: "plist") {
                if let sampleFeed = NSDictionary(contentsOfFile: sampleFeedPath) as? [String: AnyObject] {
                    completion([Feed(sampleFeed)], nil)
                    return
                }
            }
            completion([Feed([String: AnyObject]())], nil)
            
            break
        case .error:
            completion(nil, NSError() as Error)
            break
        }
    }
}
