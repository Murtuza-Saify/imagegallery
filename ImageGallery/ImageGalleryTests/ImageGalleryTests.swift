//
//  ImageGalleryTests.swift
//  ImageGalleryTests
//
//  Created by Murtuza Saify on 04/11/17.
//  Copyright © 2017 Saify. All rights reserved.
//

import XCTest
@testable import ImageGallery

class ImageGalleryTests: XCTestCase {
    var viewController: MSImageGalleryViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        viewController =  UIStoryboard(name: "Main", bundle: Bundle(for: self.classForCoder)).instantiateViewController(withIdentifier: "MSImageGalleryViewController") as! MSImageGalleryViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testSceneSetup() {
        let interactor = viewController.interactor as! MSImageGalleryInteractor
        XCTAssertNotNil(interactor)

        let presenter = interactor.presenter as! MSImageGalleryPresenter
        XCTAssertNotNil(presenter)

        let router = viewController.router as! MSImageGalleryRouter
        XCTAssertNotNil(router)
        XCTAssertEqual(presenter.viewController as? MSImageGalleryViewController, self.viewController)
        XCTAssertEqual(router.viewController, self.viewController)
    }
    
    func testViewModelForValidFetchResults() {
        let interactor = viewController.interactor as! MSImageGalleryInteractor
        XCTAssertNotNil(interactor)
        
        let worker = interactor.worker
        let mockServiceExecuter = MockServiceExecuter()
        mockServiceExecuter.mockState = .validData
        worker.serviceExecuter = mockServiceExecuter
        
        viewController.fetchFeeds()
        XCTAssertTrue(viewController.viewModel!.displayItems.count > 0)
    }
    
    func testViewModelForFetchError() {
        let interactor = viewController.interactor as! MSImageGalleryInteractor
        XCTAssertNotNil(interactor)
        
        let worker = interactor.worker
        let mockServiceExecuter = MockServiceExecuter()
        mockServiceExecuter.mockState = .error
        worker.serviceExecuter = mockServiceExecuter
        
        viewController.fetchFeeds()
        XCTAssertNil(viewController.viewModel)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
